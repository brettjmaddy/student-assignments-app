﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StudentAssignmentsApp
{
    /// <summary>
    /// This partial class is the functionality of a Student/Assignment WPF program. The user will be able
    /// to enter information about students and assignments and display the results.
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Attributes

        /// <summary>
        /// iNumOfStudents is an inte that stores the number of students the user wishes to enter.
        /// Its purpose is to determine the size of the array that will store all students, and the
        /// amount of columns in a 2D assignments array. See arrAssignments for more details on that.
        /// </summary>
        int iNumOfStudents;

        /// <summary>
        /// iNumOfAssignments is an int that stores the number of assignments the user wishes to enter.
        /// Its purpose is to determine the number of rows in the 2D assignments array. See arrAssignments
        /// for more on that.
        /// </summary>
        int iNumOfAssignments;

        /// <summary>
        /// iAssignmentNum is an int that stores a particular assignment number that the user wishes to alter.
        /// Its purpose is to store the value of an int.TryParse of a text box input. 
        /// </summary>
        int iAssignmentNum;

        /// <summary>
        /// iAssingmentScore is an int that stores the value of an assignments score. Its purpose is to store
        /// the value of an int.TryParse of a text box.
        /// </summary>
        int iAssignmentScore;

        /// <summary>
        /// iNextStudent is an int that stores which student is being selected and altered with the first, next, 
        /// previous, and last buttons. Its purpose is for convenience so that we don't have to keep referencing
        /// arrStudents. Its value is updated as the list of students is traversed.
        /// </summary>
        int iNextStudent = 0;

        /// <summary>
        /// iTotalScore is an int value that stores the total of all assignment scores for a student and will be
        /// used to calculate the average.
        /// </summary>
        int iTotalScore;

        /// <summary>
        /// iAvgGrade is an double value that stores the average score for a student, and will be displayed
        /// in the txtDisplayScores TextBox when the btnDisplayScores button is clicked.
        /// </summary>
        double iAvgGrade;

        /// <summary>
        /// sLetterGrade is a string value that stores the letter grade for each student and will be displayed
        /// in the txtDisplayScores TextBox when the btnDisplayScores button is clicked.
        /// </summary>
        string sLetterGrade;

        /// <summary>
        /// sDisplayInfo is a string value that is used to display student name and grade info in the txtDisplayScores
        /// TextBox when the btnDisplayScores button is clicked. There will be concatentations done on the string,
        /// and other string values assigned to it dynamically, so as not to needlessly create multiple string instances.
        /// </summary>
        string sDisplayInfo;

        /// <summary>
        /// isSubmitted is a boolean value that is used todetermine whether or not the user has already submitted 
        /// student and assignment counts. It they have, the state of this boolean will prevent them from submitting
        /// more than once without first resetting the app.
        /// </summary>
        Boolean isSubmitted = false;

        /// <summary>
        /// arrStudents is a string array that stores all of the students. The default name for each student is Student #_
        /// until the user changes the names.
        /// </summary>
        string[] arrStudents;

        /// <summary>
        /// arrAssignments is a 2D int array that stores the assignments and each assignment's scores. Each assingment number
        /// is represented in the row portion of the 2D array with the index number of each row. The column portion of the array
        /// is where each assignment score is stored for each student. That is, Student #5's score for Assignment #3 would be
        /// stored in arrAssignment[2, 4] (subtracting 1 from each because of 0 indexing). Row two is StudentAssignmentsApp, and column 4
        /// is the fifth "copy" of the assignment that belongs to student 5.
        /// </summary>
        int[,] arrAssignments;        

        #endregion

        #region Methods

        /// <summary>
        /// Default constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// btnSubmitCounts_Click is an event handler for btnSubmitCounts. When the user enters in the number of
        /// students and the number of assignments several conditions will be checked. See the comments within the
        /// method for more detail.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmitCounts_Click(object sender, RoutedEventArgs e)
        {
            // If the boolean isSubmitted = true, then user will be told that they have already sumbitted data
            // and must reset the app if they wish to enter new data.
            if (isSubmitted == true)
            {
                MessageBox.Show("You have already submitted number of students \nand assignments." +
                                "You must click RESET SCORES if \nyou wish to enter new data.",
                                "Already Submitted", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                // This fat mess of if-else statements is checking to see if the user has entered valid input. 
                // There can be nothing but integers entered into the text box. Also checking to see if the user
                // entered in an integer value beyond the limits allowed. A message box appears if the parsing 
                // fails, the integer values are too great, or <= 0.
                if(!int.TryParse(txtNumOfStudents.Text, out iNumOfStudents) ||
                    !int.TryParse(txtNumOfAssignments.Text, out iNumOfAssignments))
                {
                    MessageBox.Show("You must enter a valid number value.\n\nNumber of Students: 1-10\nNumber of Assignments: 1-99", 
                        "Count Input Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if(iNumOfStudents > 10 || iNumOfAssignments > 99)
                {
                    MessageBox.Show("You must enter a valid number value.\n\nNumber of Students: 1-10\nNumber of Assignments: 1-99",
                        "Count Input Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }                
                else if(iNumOfStudents <= 0 || iNumOfAssignments <= 0)
                {
                    MessageBox.Show("You must enter a valid number value.\n\nNumber of Students: 1-10\nNumber of Assignments: 1-99",
                        "Count Input Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                { 
                    // Instantiating arrStudents and arrAssignments. Their sizes will depend on what the user enters for
                    // number of students and number of assignments.
                    arrStudents = new string[iNumOfStudents];
                    arrAssignments = new int[iNumOfAssignments, iNumOfStudents];

                    // isSubmitted is set to true, confirming that the user has submitted number of students
                    // and number of assignments and will need to reset the app before submitting this data again.
                    isSubmitted = true;
                
                    // Setting the defualt name for each student in the array to Student #_
                    // The user can change this later.
                    for(int i = 0; i < arrStudents.Length; i++)
                    {
                        arrStudents[i] = "Student #" + (i + 1);
                    }

                    // Setting the default score to all the assignments to 0. The user can change this later.                   
                    for (int row = 0; row < arrAssignments.GetLength(0); row++)
                    {
                        for(int col = 0; col < arrAssignments.GetLength(1); col++)
                        {
                            arrAssignments[row, col] = 0;
                        }
                    }

                    // Displaying the interval of assignment numbers, 1 - iNumOfAssignments, that are available to edit
                    // on the lblEnterAssignmentNum label.
                    lblEnterAssignmentNum.Content = "Enter Assignment Number (1-" + iNumOfAssignments + "):";

                    // Removing the text from the text boxes for visual purposes.
                    txtNumOfStudents.Text = "";
                    txtNumOfAssignments.Text = "";
                }
            }
        }

        /// <summary>
        /// btnFirstStudent_Click is the event handler for the btnFirstStudent button. When it is clicked,
        /// the user will be taken to the first student is arrStudents. The user can change the student's
        /// name and the updated name will be displayed on lblStudentName label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFirstStudent_Click(object sender, RoutedEventArgs e)
        {
            // Confirming that the user has first submitted count values.
            if (isSubmitted == true)
            {
                // Displaying the name of the first student on the lblStudentName label.
                lblStudentName.Content = arrStudents[0];

                // Setting iNextStudent to 0, which is the first student in arrStudents
                iNextStudent = 0;
            }
        }

        /// <summary>
        /// btnPrevStudent_Click is the event handler for the btnPrevStudent button. When it is clicked, the user 
        /// will be taken to the previous contiguous student in arrStudents. The user can change the student's
        /// name and the updated name will be displayed on lblStudentName label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrevStudent_Click(object sender, RoutedEventArgs e)
        {
            // Confirming that the user has first submitted count values. 
            if (isSubmitted == true)
            {
                // Decrement iNextStudent only if we are not already at the beginning of arrStudents. This
                // prevents going out of bounds.
                if (iNextStudent != 0)
                {
                    iNextStudent--;
                }

                // Displaying the name of the previous student on the lblStudentName label.
                lblStudentName.Content = arrStudents[iNextStudent];
            }
        }

        /// <summary>
        /// btnNextStudent_Click is the event handler for the btnNextStudent button. When it is clicked, the user 
        /// will be taken to the next contiguous student in arrStudents. The user can change the student's
        /// name and the updated name will be displayed on lblStudentName label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNextStudent_Click(object sender, RoutedEventArgs e)
        {
            // Confirming that the user has first submitted count values.
            if (isSubmitted == true)
            {
                // Increment iNextStudent only if we are not already at the end of arrStudents. This
                // prevents going out of bounds.
                if (iNextStudent != arrStudents.Length - 1)
                {
                    iNextStudent++;
                }

                // Displaying the name of the next student on the lblStudentName label.
                lblStudentName.Content = arrStudents[iNextStudent];
            }
        }

        /// <summary>
        /// btnLastStudent_Click is the event handler for the btnLastStudent button. When it is clicked, the user
        /// will be taken to the last student in arrStudents. The user can change the student's
        /// name and the updated name will be displayed on lblStudentName label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLastStudent_Click(object sender, RoutedEventArgs e)
        {
            // Confirming that the user has first submitted count values.
            if (isSubmitted == true)
            {
                // Displaying the name of the last student on the lblStudentName label.
                lblStudentName.Content = arrStudents[iNumOfStudents - 1];
                
                // Setting iNextStudent to the value of the lsat index of the array, which is the last student
                // in arrStudents.
                iNextStudent = arrStudents.Length - 1;
            }
        }


        /// <summary>
        /// btnSaveName_Click is the event handler for the btnSaveName button. When it is clicked, the name entered into
        /// txtStudentName text box will be assigned as the nam of the student that is being selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveName_Click(object sender, RoutedEventArgs e)
        {
            // Confirming that the user has first submitted count values.
            if (isSubmitted == true)
            {
                // Preventing blank space in txtStudentName from being accepted. If the user has not entered anything into
                // the text box, a MEssageBox is displayed.
                if (txtStudentName.Text == "")
                {
                    MessageBox.Show("You must enter in a valid name", "Enter a Valid Name", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    // Setting the new of the the student that is begin selected.
                    arrStudents[iNextStudent] = txtStudentName.Text;

                    // Displaying the new name in the lblStudentName label.
                    lblStudentName.Content = txtStudentName.Text;

                    // Removing the text from the text box for visual purposes.
                    txtStudentName.Text = "";
                }
            }
        }

        /// <summary>
        /// btnSaveScore_Click is the event handler for the btnSaveScore button. When it is clicked, the values entered into
        /// the txtAssignmentScore text box will be assigned to the assignment number entered into the txtAssignmentNum text box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveScore_Click(object sender, RoutedEventArgs e)
        {
            // Confirming that the user has first submitted count values.
            if (isSubmitted == true)
            {
                // Checking to see if the user has entered valid input. There can be nothing but integers entered into the text 
                // boxes. Also checking to see if the user entered in an assignment number greater than the number of assignments
                // that exists, or if a value of 0 or lower has been entered. A message box appears if the parsing 
                // fails, the integer values are too great, or <= 0.
                if (!int.TryParse(txtAssignmentNum.Text, out iAssignmentNum) || 
                    !int.TryParse(txtAssignmentScore.Text, out iAssignmentScore))
                {
                    MessageBox.Show("You must enter a valid number value.\n\nAssignment Number: 1-" + iNumOfAssignments + "\nAssignment Score: 1-100",
                        "Assignment Input Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if(iAssignmentNum > iNumOfAssignments || iAssignmentNum <= 0)
                {
                    MessageBox.Show("You must enter a valid number value.\n\nAssignment Number: 1-" + iNumOfAssignments + "\nAssignment Score: 1-100",
                        "Assignment Input Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    // The value of iAssignmentNum and iNextStudent are used to locate the appropriate assignment to set the score for. 
                    arrAssignments[iAssignmentNum - 1, iNextStudent] = iAssignmentScore;
                    
                    // Removing the text from the txtAssignmentNum and txtAssignmentScore text boxes for visual purposes.
                    txtAssignmentNum.Text = "";
                    txtAssignmentScore.Text = "";
                }
            }
        }

        /// <summary>
        /// btnDisplayScores_Click is the event handler for the btnDisplayScores button. When it is clicked, the scores for
        /// each assignment, the average score/grade value, and letter grade for each student is displayed in the txtDisplayScores
        /// TextBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDisplayScores_Click(object sender, RoutedEventArgs e)
        {


            // Confirming that the user has first submitted count values.
            if (isSubmitted == true)
            {
                // This is the first portion of a "header" that will sit above the student names and assignment scores.
                sDisplayInfo = "Student\t\t ";

                // Looping through each assignment number and concatenating "#n" to the sDisplayInfo string. This portion
                // is the rest of the "header".
                for (int i = 0; i < arrAssignments.GetLength(0); i++)
                {
                    sDisplayInfo += "#" + (i + 1) + "\t";
                }

                // The tale end of the "header" concatenated with the sDisplayInfo string.
                sDisplayInfo += " AVG\t Grade \n";

                // Adding the sDisplayInfo string to the txtDisplayScores text box for display.
                
                txtDisplayScores.Text = sDisplayInfo;

                // Looping through the student array to print each student's name and subsequent assigmment/grade
                // info on their own lines.
                for (int i = 0; i < arrStudents.Length; i++)
                {
                    // New line character, student name, and tab concatenated with the sDisplayInfo string.
                    sDisplayInfo = "\n" + arrStudents[i] + "\t";

                    // Looping through arrStudents to get each score for each assignment for each student.
                    for (int row = 0; row < arrAssignments.GetLength(0); row++)
                    {
                        // Concatenating each score to the sDisplayInfo string.
                        sDisplayInfo += arrAssignments[row, i] + "\t";

                        // Getting a total score for each student to be used to calculate the average and letter grade.
                        iTotalScore += arrAssignments[row, i];
                    }

                    // Calculating the average grade.
                    iAvgGrade = (double)iTotalScore / (double)iNumOfAssignments;

                    // Concatenating the average grade, and the letter grade to the sDisplayInfo string.
                    // CalculateLetterGrade is a helper method that determines the leter grade based on average
                    // grade score. It returns a string, which is the letter grade. See below for details on this
                    // helper method.
                    sDisplayInfo += iAvgGrade + "\t" + CalculateLetterGrade(iAvgGrade) + "\n";

                    // Resetting iTotalScore to 0 so that it is ready to start being added to for the next student.
                    iTotalScore = 0;

                    // Adding the sDisplayInfo string to the txtDisplayScores TextBox, creating individual lines
                    // for each student.
                    txtDisplayScores.AppendText(sDisplayInfo);
                }
            }
        }

        /// <summary>
        /// btnResetScores_Click is the event handler for the btnResetScores button. When it is clicked, the current state of the
        /// application will be reset to original. All values stored in the program will be erased and the user will be able to enter
        /// in new data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnResetScores_Click(object sender, RoutedEventArgs e)
        {
            // Checking to see if the user really wants to reset scores. Their response is stored and that value will determine
            // is resetting happens, or the program is left the same.
            var result = MessageBox.Show("Are you sure you want to reset the scores? All current data will be lost.", "Reset Confirm", 
                                            MessageBoxButton.YesNo, MessageBoxImage.Question);

            // Zero-ing out all the data in the app, starting fresh.
            if (result == MessageBoxResult.Yes)
            {
                iNumOfStudents = 0;
                iNumOfAssignments = 0;
                iAssignmentNum = 0;
                iAssignmentScore = 0;
                iNextStudent = 0;
                isSubmitted = false;
                arrStudents = null;
                arrAssignments = null;
                lblStudentName.Content = "";
                txtAssignmentNum.Text = "";
                txtDisplayScores.SelectAll();
                txtDisplayScores.Text = "";
            }
        }

        /// <summary>
        /// CalculateLetterGrade() is a helper method that isn't exactly neccessary, but helps to clean up some code.
        /// It exists primarily to reduce the lines of code in the btnDisplayScores_Click method. It assigns a letter
        /// grade based on the average score for a student
        /// </summary>
        /// <param name="avg"></param>
        /// <returns></returns>
        private string CalculateLetterGrade(double avg)
        {
            if (iAvgGrade >= 93)
            {
                sLetterGrade = "A";
            }

            if (iAvgGrade < 93 && iAvgGrade >= 90)
            {
                sLetterGrade = "A-";
            }

            if (iAvgGrade < 90 && iAvgGrade >= 87)
            {
                sLetterGrade = "B+";
            }

            if (iAvgGrade < 87 && iAvgGrade >= 83)
            {
                sLetterGrade = "B";
            }

            if (iAvgGrade < 83 && iAvgGrade >= 80)
            {
                sLetterGrade = "B-";
            }

            if (iAvgGrade < 80 && iAvgGrade >= 77)
            {
                sLetterGrade = "C+";
            }

            if (iAvgGrade < 77 && iAvgGrade >= 73)
            {
                sLetterGrade = "C";
            }

            if (iAvgGrade < 73 && iAvgGrade >= 70)
            {
                sLetterGrade = "C-";
            }

            if (iAvgGrade < 70 && iAvgGrade >= 67)
            {
                sLetterGrade = "D+";
            }

            if (iAvgGrade < 67 && iAvgGrade >= 63)
            {
                sLetterGrade = "D";
            }

            if (iAvgGrade < 63 && iAvgGrade >= 60)
            {
                sLetterGrade = "D-";
            }

            if (iAvgGrade < 60)
            {
                sLetterGrade = "F";
            }

            return sLetterGrade;
        }



        #endregion
    }
}
